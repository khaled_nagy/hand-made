import 'package:flutter/material.dart';
import 'package:hand_made/drawer.dart';
import 'package:hand_made/focushelpwidget.dart';

import 'package:mvc_pattern/mvc_pattern.dart';

import 'package:hand_made/src/controllers/family_producer/AddProductController.dart';

class AddProductScreen extends StatefulWidget {
  createState() => AddProductView();
}

class AddProductView extends StateMVC<AddProductScreen> {
  AddProductView() : super(AddProductController()) {
    _addProductController = AddProductController.con;
  }
  AddProductController _addProductController;

  String dropDownTitleCategory = "974";
  String _valueCategory;

  List<String> categories = [
    '123',
    '456',
    '789 ',
    '741',
  ];
  String sizeCategory = "";

  FocusNode _descfocus = new FocusNode();
  FocusNode _descfocus1 = new FocusNode();
  TextEditingController _JopDescriptionContoller = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        endDrawer: DrawerW().showDrawerUser(context),
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        title: Text(
          "اضف منتج جديد",
          style: TextStyle(
            color: Colors.grey[800],
          ),
        ),
        backgroundColor: Colors.grey[50],
        leading: InkWell(
          child: Icon(
            Icons.arrow_back,
            color: Colors.grey[800],
          ),
          onTap: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              image: new DecorationImage(
                  image: new AssetImage("assets/imgs/ic_decoration.png"),
                  fit: BoxFit.cover)),
          child: Padding(
            padding: const EdgeInsets.only(left: 32, right: 32),
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height / 25,
                  ),
                  Row(
                    children: <Widget>[
                      Image.asset(
                        "assets/imgs/ic_upload_avatar.png",
                        width: MediaQuery.of(context).size.width / 3.5,
                        height: MediaQuery.of(context).size.height / 6,
                      ),
                    ],
                  ),
                  new Container(
                    decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Colors.grey[300]),
                        borderRadius: BorderRadius.circular(12)),
                    height: 50,
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: Container(
                            width: MediaQuery.of(context).size.width / 1.5,
                            child: new TextField(
                              keyboardType: TextInputType.text,
                              controller: TextEditingController(),
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.only(
                                    right: 4.0, top: 0.0, left: 4),
                                hintText: "اسم المنتج",
                                hintStyle:
                                    new TextStyle(color: Colors.grey[400]),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  new Container(
                    height: 16,
                  ),
                  new Container(
                    width: MediaQuery.of(context).size.width,
                    height: 50.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      border: Border.all(width: 1, color: Colors.grey[300]),
                    ),
                    child: Padding(
                        padding:
                            const EdgeInsets.only(top: 6, left: 16, right: 16),
                        child: GestureDetector(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                "القسم",
                                style: TextStyle(color: Colors.grey[500]),
                              ),
                              Image.asset("assets/imgs/ic_arrow_down.png")
                            ],
                          ),
                          onTap: () {
                            _addProductController
                                .openBottomSheetDepartment(context);
                          },
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Container(
                          decoration: BoxDecoration(
                              border:
                                  Border.all(width: 1, color: Colors.grey[300]),
                              borderRadius: BorderRadius.circular(12)),
                          height: 50,
                          width: MediaQuery.of(context).size.width / 2.5,
                          child: Row(
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width / 4,
                                child: new TextField(
                                  keyboardType: TextInputType.text,
                                  controller: TextEditingController(),
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    contentPadding: EdgeInsets.only(
                                        right: 16.0, top: 0.0, left: 16),
                                    hintText: "السعر",
                                    hintStyle:
                                        new TextStyle(color: Colors.grey[400]),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        new Container(
                          width: MediaQuery.of(context).size.width / 2.5,
                          height: 50.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            border:
                                Border.all(width: 1, color: Colors.grey[300]),
                          ),
                          child: Padding(
                              padding: const EdgeInsets.only(top: 6),
                              child: GestureDetector(
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: <Widget>[
                                    Text(
                                      "مدة التسليم",
                                      style: TextStyle(color: Colors.grey[400]),
                                    ),
                                    Image.asset("assets/imgs/ic_arrow_down.png")
                                  ],
                                ),
                                onTap: () {
                                  _addProductController.openBottomSheetDelevrytime(context);
                                },
                              )),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 16, bottom: 4, left: 8, right: 8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("وصف المنتج",
                            style: TextStyle(color: Colors.grey[800])),
                        Text("200 كلمه",
                            style: TextStyle(color: Colors.grey[500])),
                      ],
                    ),
                  ),
                  new Container(
                    height: 88.0,
                    child: new Card(
                      shape: RoundedRectangleBorder(
                        
                          borderRadius: BorderRadius.circular(12.0)),
                      color: Colors.white,
                      elevation: 0,
                      child: new Column(
                        children: <Widget>[
                          new Container(
                            // margin: new EdgeInsets.all(16.0),
                            width: 360.0,
                            height: 80.0,
                            decoration: new BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8.0)),
                                shape: BoxShape.rectangle,
                                border: new Border(
                                top: new BorderSide(
                                    width: 1.0, color: Colors.grey[300]),
                                left: new BorderSide(
                                    width: 1.0, color: Colors.grey[300]),
                                right: new BorderSide(
                                    width: 1.0, color: Colors.grey[300]),
                                bottom: new BorderSide(
                                    width: 1.0, color: Colors.grey[300]),
                              )),
                            child: new EnsureVisibleWhenFocused(
                                focusNode: _descfocus,
                                child: TextFormField(
                                  focusNode: _descfocus,
                                  style: new TextStyle(color: Colors.grey[800]),
                                  obscureText: false,
                                  controller: _JopDescriptionContoller,
                                  maxLength: 700,
                                  maxLines: 8,
                                  decoration: new InputDecoration.collapsed(
                                      hintText: " اكتب وصفك",
                                      hintStyle: new TextStyle(
                                          color: Colors.grey[500],
                                          fontFamily: 'JF Flat')),
                                )),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                          new Container(
                          width: MediaQuery.of(context).size.width / 2.5,
                          height: 50.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            border:
                                Border.all(width: 1, color: Colors.grey[300]),
                          ),
                          child: Padding(
                              padding: const EdgeInsets.only(top: 6),
                              child: GestureDetector(
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: <Widget>[
                                    Text(
                                      "توصيل",
                                      style: TextStyle(color: Colors.grey[400]),
                                    ),
                                    Image.asset("assets/imgs/ic_arrow_down.png")
                                  ],
                                ),
                                onTap: () {
                                  _addProductController.openBottomSheetDelevry(context);
                                },
                              )),
                        ),
                        new Container(
                          decoration: BoxDecoration(
                              border:
                                  Border.all(width: 1, color: Colors.grey[200]),
                              borderRadius: BorderRadius.circular(12)),
                          height: 50,
                          width: MediaQuery.of(context).size.width / 2.5,
                          child: Row(
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width / 4,
                                child: new TextField(
                                  keyboardType: TextInputType.text,
                                  controller: TextEditingController(),
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    contentPadding: EdgeInsets.only(
                                        right: 16.0, top: 0.0, left: 16),
                                    hintText: "السعر",
                                    hintStyle:
                                        new TextStyle(color: Colors.grey[300]),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      
                      ],
                    ),
                  ),

                  Container(
                    height: 16,
                  ),
                  InkWell(
                    child: new Container(
                      height: 45.0,
                      width: MediaQuery.of(context).size.width,
                      child: new Material(
                          color: const Color(0xffA60A53).withOpacity(0.2),
                          elevation: 0.0,
                          shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(12.0),
                          ),
                          child: new Center(
                              child: new Padding(
                                  padding: new EdgeInsets.only(
                                      top: 0.0, bottom: 0.0),
                                  child: new Text(
                                    "اضف المنتج",
                                    style: new TextStyle(
                                        color: Colors.white,
                                        fontSize: 18.0,
                                        fontFamily: 'JF Flat'),
                                  )))),
                    ),
                    onTap: () {},
                  ),
                  Container(
                    height: 16,
                  ),
                ],
              ),
            ),
          )),
    );
  }
}
