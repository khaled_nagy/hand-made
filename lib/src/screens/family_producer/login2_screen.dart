import 'package:flutter/material.dart';
import 'package:hand_made/drawer.dart';
import 'package:hand_made/src/controllers/family_producer/Login2Controller.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:hand_made/src/screens/consumer_screens/bottom_navigation_bar_consumer_screen.dart';

class Login2Screen extends StatefulWidget {
  createState() => Login2View();
}

class Login2View extends StateMVC<Login2Screen> {
  Login2View() : super(Login2Controller()) {
    _login2controller = Login2Controller.con;
  }
  Login2Controller _login2controller;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        endDrawer: DrawerW().showDrawerUser(context),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage("assets/imgs/ic_decoration.png"),
                fit: BoxFit.cover)),
        child: Padding(
          padding: const EdgeInsets.only(left: 32, right: 32),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height / 25,
                ),
                Image.asset(
                  "assets/imgs/production_famely.PNG",
                  width: MediaQuery.of(context).size.width / 2,
                  height: MediaQuery.of(context).size.height / 3,
                ),
                Container(
                  height: MediaQuery.of(context).size.height / 25,
                ),
                Text("ادخل بياناتك", style: TextStyle(fontSize: 18)),
                Container(
                  height: 33,
                ),
                new Container(
                  decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Colors.grey[300]),
                      borderRadius: BorderRadius.circular(12)),
                  height: 50,
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding:
                            const EdgeInsets.only(top: 0, right: 16, left: 16),
                        child: Image.asset("assets/imgs/ic_username.png" , width: 20,height: 20,),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 1.5,
                        child: new TextField(
                          keyboardType: TextInputType.text,
                          controller: TextEditingController(),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            contentPadding:
                                EdgeInsets.only(right: 4.0, top: 0.0, left: 4),
                            hintText: "اسم المستخدم",
                            hintStyle: new TextStyle(color: Colors.grey[400]),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  height: 15,
                ),
                new Container(
                  decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Colors.grey[300]),
                      borderRadius: BorderRadius.circular(12)),
                  height: 50,
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding:
                            const EdgeInsets.only(top: 0, right: 16, left: 16),
                        child: Image.asset("assets/imgs/ic_lock.png", width: 20,height: 20,),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 1.5,
                        child: new TextField(
                          keyboardType: TextInputType.text,
                          controller: TextEditingController(),
                          obscureText: true,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            contentPadding:
                                EdgeInsets.only(right: 4.0, top: 0.0, left: 4),
                            hintText: "كلمة المرور",
                            hintStyle: new TextStyle(color: Colors.grey[400]),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  height: 16,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    InkWell(
                      child: new Container(
                        height: 45.0,
                        width: MediaQuery.of(context).size.width / 3,
                        child: new Material(
                            color: const Color(0xffA60A53),
                            elevation: 0.0,
                            shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(12.0),
                            ),
                            child: new Center(
                                child: new Padding(
                                    padding: new EdgeInsets.only(
                                        top: 0.0, bottom: 0.0),
                                    child: new Text(
                                      "دخول",
                                      style: new TextStyle(
                                          color: Colors.white,
                                          fontSize: 18.0,
                                          fontFamily: 'JF Flat'),
                                    )))),
                      ),
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(
                          builder: (BuildContext context)=>BottomNavigationBarConsumer()
                        ));
                      },
                    ),
                    new Text(
                      "نسيت كلمة المرور ؟",
                      style: new TextStyle(
                          color: Colors.grey[500],
                          fontSize: 18.0,
                          fontFamily: 'JF Flat'),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
