import 'package:flutter/material.dart';

import 'package:mvc_pattern/mvc_pattern.dart';

import 'package:hand_made/src/controllers/family_producer/HomeController.dart';

import 'package:flutter_rating/flutter_rating.dart';

import 'package:hand_made/src/widgets/HomeCard.dart';

import 'package:hand_made/src/screens/family_producer/add_product_screen.dart';

import 'package:hand_made/drawer.dart';

class HomeScreen extends StatefulWidget {
  createState() => HomeView();
}

class HomeView extends StateMVC<HomeScreen> {
  HomeView() : super(HomeController()) {
    _homeController = HomeController.con;
  }
  HomeController _homeController;

  double seleListRatingHearts = 3.5;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
     endDrawer: DrawerW().showDrawerUser(context),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.only(
            left: 16,
            top: 50,
            right: 16,
          ),
          child: Column(
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  InkWell(
                    child: Image.asset("assets/imgs/ic_add.png",width: 30,height: 30,),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  AddProductScreen()));
                    },
                  ),
                  CircleAvatar(
                    radius: 40,
                    backgroundColor: Colors.grey[400],
                  ),
                  Image.asset("assets/imgs/side_menu.png",width: 30,height: 30,),
                ],
              ),
              Text(
                "مروة للكروشية",
                style: TextStyle(color: Colors.grey[500], fontSize: 18),
              ),
              new StarRating(
                rating: seleListRatingHearts,
                size: 18,
                color: const Color(0xffA60A53),
                borderColor: Colors.grey,
                starCount: 5,
                onRatingChanged: (_rating) => setState(
                      () {
                        this.seleListRatingHearts = _rating;
                      },
                    ),
              ),
              Text(
                "حيث ننتقل من تعريفكم بالفن الى ارساله لمنازلكم مباشرة ! هذا المتجر خاص بموقع وقناة الاعمال اليدوية للجميع .",
                style: TextStyle(color: Colors.grey[500], fontSize: 15),
              ),
              Expanded(
                child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                  ),
                  physics: AlwaysScrollableScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  itemCount: 6,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    return HomeCard();
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}


