import 'package:flutter/material.dart';
import 'package:hand_made/drawer.dart';

import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:hand_made/src/controllers/family_producer/NotificationController.dart';

import 'package:hand_made/src/widgets/NotificationCard.dart';

class NotificationScreen extends StatefulWidget {
  createState() => NotificationView();
}

class NotificationView extends StateMVC<NotificationScreen> {
  NotificationView() : super(NotificationController()) {
    _notificationController = NotificationController.con;
  }
  NotificationController _notificationController;

 

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        endDrawer: DrawerW().showDrawerUser(context),
      appBar: AppBar(
        centerTitle: true,
        title: Text("الاشعارات" , style: TextStyle(color: Colors.grey[800]),),
        backgroundColor: Colors.grey[50],
        elevation: 0,
      ),
      body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              image: new DecorationImage(
                  image: new AssetImage("assets/imgs/ic_decoration.png"),
                  fit: BoxFit.cover)),
          child: Padding(
            padding: const EdgeInsets.only(left: 32, right: 32),
            child:  Column(
                children: <Widget>[

                  ListView.builder(
                    shrinkWrap: true,
                    itemCount: 2,
                    itemBuilder: (BuildContext context , index){
return NotificationCard();
                    },
                  )
                
                ],
              ),
            
          )),
    );
  }
}
