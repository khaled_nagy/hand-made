import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_verification_code_input/flutter_verification_code_input.dart';
import 'package:hand_made/drawer.dart';
import 'package:hand_made/src/screens/family_producer/activity_data_screen.dart';

class VerificationCode extends StatefulWidget {
  @override
  _VerificationCode createState() => new _VerificationCode();
}

class _VerificationCode extends State<VerificationCode> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        endDrawer: DrawerW().showDrawerUser(context),
      body: new Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage("assets/imgs/ic_decoration.png" ), fit: BoxFit.cover)),
        child: Padding(
          padding: const EdgeInsets.only(top: 30.0, left: 32.0, right: 32.0),
          child: new SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Container(
              height: MediaQuery.of(context).size.height,
              child: Column(
                children: <Widget>[

                  Padding(
                    padding: const EdgeInsets.fromLTRB(0 ,150 , 0 , 8),
                    child: Row(
                      children: <Widget>[
                        Text("تاكيد الكود" ,style: TextStyle(color: Colors.grey[900] , fontSize: 18 , ),),
                      ],
                    ),
                  ),

                   Padding(
                    padding: const EdgeInsets.fromLTRB(0 ,0 , 0 , 8),
                    child: Row(
                      children: <Widget>[
                        SizedBox(
                          height: 60,
                          width: MediaQuery.of(context).size.width/1.4,
                                                  child: Text("ادخل رمز التحقق للرسالةالتى ستتلقاها على هاتفك" 
                          ,style: TextStyle(color: Colors.grey[500] , fontSize: 15 , ),),
                        ),
                      ],
                    ),
                  ),

                  Directionality(
                    textDirection: TextDirection.ltr,
                                      child: Row(
                      
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text("+966543214556" 
                            ,style: TextStyle(color: Colors.grey[500] , fontSize: 18 , ),),

                            Container(width: 8,),

                            Image.asset("assets/imgs/ic_edit.png" , width: 30,height: 30,)
                      ],
                    ),
                  ),

                  
                 
                  Container(
                    height: MediaQuery.of(context).size.height / 4,
                    child: Directionality(
                      textDirection: TextDirection.ltr,
                      child: new VerificationCodeInput(
                          keyboardType: TextInputType.number,
                          textStyle: TextStyle(),
                          length: 4,
                          onCompleted: (String value) {
                            print(value);

                            showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  content: Text('يتم تاكيد العضو....'),
                                );
                              },
                            );
                          }),
                    ),
                  ),

                    InkWell(
                    child: new Container(
                      height: 45.0,
                      width: MediaQuery.of(context).size.width,
                      child: new Material(
                          color: const Color(0xffA60A53),
                          elevation: 0.0,
                          shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(12.0),
                          ),
                          child: new Center(
                              child: new Padding(
                                  padding: new EdgeInsets.only(
                                      top: 0.0, bottom: 0.0),
                                  child: new Text(
                                    "التالى",
                                    style: new TextStyle(
                                        color: Colors.white,
                                        fontSize: 18.0,
                                        fontFamily: 'JF Flat'),
                                  )))),
                    ),
                    onTap: () {

                      Navigator.push(context, MaterialPageRoute(
                        builder: (BuildContext context)=> ActivityDataScreen()
                      ));
                    },
                  ),


                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Directionality(
                      textDirection: TextDirection.ltr,
                      
                      child: Text("5 : 0" , style: TextStyle(fontSize: 18 , color: Colors.grey[500] , ),
                      )
                      ),
                  ),


              Padding(
                padding: const EdgeInsets.only(top: 50),
                child: Text("اعادة ارسال رمز التنشيط" , style: TextStyle(fontSize: 15 , color:const Color(0xffA60A53) , ),
                        ),
              )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
