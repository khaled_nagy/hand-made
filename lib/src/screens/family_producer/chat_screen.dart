import 'package:flutter/cupertino.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:hand_made/drawer.dart';
import 'package:hand_made/src/controllers/family_producer/ChatController.dart';

import 'package:flutter/material.dart';

import 'package:mvc_pattern/mvc_pattern.dart';

class ChatScreen extends StatefulWidget {
  createState() => ChatView();
}

class ChatView extends StateMVC<ChatScreen> with TickerProviderStateMixin {
  ChatView() : super(ChatController()) {
    _chatController = ChatController.con;
  }

  ChatController _chatController;
  final TextEditingController _textController = new TextEditingController();
  final List<Msg> _message = <Msg>[];
  bool _isWriting = false;
  double rating = 3.5 ;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      drawer: new DrawerW().showDrawerUser(context),
      appBar: new AppBar(
        centerTitle: true,
        
        title: new Text("محادثه" , style: TextStyle(color: Colors.grey[800]),),
        leading: InkWell(child: Icon(Icons.arrow_back , color: Colors.grey[800],),onTap: (){
          Navigator.pop(context);
        },),
        backgroundColor: Colors.grey[50],
        elevation: 1,
      ),
      body: new Column(children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            children: <Widget>[

              InkWell(child: Image.asset("assets/imgs/ic_consumer.png" , width: 100,height: 100, ),onTap: (){
                _chatController.openBottomSheetRating(context, rating);
              },)
            ],
          ),
        ),
        new Flexible(
            child: new ListView.builder(
          itemCount: _message.length,
          reverse: true,
          padding: new EdgeInsets.all(6.0),
          itemBuilder: (_, index) {
            return _message[index];
          },
        )),
        new Divider(height: 1.0),
        new Container(
          child: _buildComposer(),
          decoration: new BoxDecoration(color: Theme.of(context).cardColor),
        ),
      ]),
    );
  }

  Widget _buildComposer() {
    return new IconTheme(
      data: new IconThemeData(color: Theme.of(context).accentColor),
      child: new Container(
        height: 50,
          margin: const EdgeInsets.symmetric(horizontal: 9.0),
          child: new Row(
            children: <Widget>[
              Icon(Icons.message , color: Colors.grey[400],),
              Container(width: 8,),
              new Flexible(
                child: new TextField(
                  controller: _textController,
                  onChanged: (String txt) {
                    setState(() {
                      _isWriting = txt.length > 0 ?_isWriting=true :_isWriting=false;
                    });
                  },
                  onSubmitted: _submitMsg,
                  decoration:
                      new InputDecoration.collapsed(hintText: "اكتب رسالتك"),
                ),
              ),
              new Container(
                  margin: new EdgeInsets.symmetric(horizontal: 3.0),
                  child: Theme.of(context).platform == TargetPlatform.iOS
                      ? new CupertinoButton(
                          child: new Text("Submit"),
                          onPressed: () => _submitMsg(_textController.text))
                      : new   InkWell(
                      child: new Container(
                        height: 40.0,
                        width: MediaQuery.of(context).size.width / 4,
                        child: new Material(
                            color: const Color(0xffA60A53),
                            elevation: 0.0,
                            shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(12.0),
                            ),
                            child: new Center(
                                child: new Padding(
                                    padding: new EdgeInsets.only(
                                        top: 0.0, bottom: 0.0),
                                    child: new Text(
                                      "ارسل",
                                      style: new TextStyle(
                                          color: Colors.white,
                                          fontSize: 18.0,
                                          fontFamily: 'JF Flat'),
                                    )))),
                      ),
                      onTap: () {
                        _submitMsg(_textController.text);
                      },
                    ),),
            ],
          ),
          decoration: Theme.of(context).platform == TargetPlatform.iOS
              ? new BoxDecoration(
                  border: new Border(top: new BorderSide(color: Colors.brown)))
              : null),
    );
  }

  void _submitMsg(String txt) {
    _textController.clear();
    setState(() {
      _isWriting = false;
    });

    Msg msg = Msg(
      txt: txt,
      animationController: AnimationController(
          vsync: this, duration: Duration(milliseconds: 800)),
    );

    setState(() {
      _message.insert(0, msg);
    });

    msg.animationController.forward();
  }

  @override
  void dispose() {
    for(Msg msg in _message ){
msg.animationController.dispose();
    }
    super.dispose();

  }
}

class Msg extends StatelessWidget {
  String txt;
  AnimationController animationController;
  Msg({this.txt, this.animationController});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    String defaultUserName = "khaled";
    return SizeTransition(
      sizeFactor: CurvedAnimation(
        curve: Curves.bounceOut,
        parent: animationController,
      ),
      axisAlignment: 0.0,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 8),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(right: 18.0),
              child: CircleAvatar(
                child: Text(defaultUserName[0]),
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    defaultUserName,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 6),
                    child: Text(txt),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
