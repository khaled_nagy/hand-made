import 'package:flutter/material.dart';
import 'package:hand_made/drawer.dart';

import 'package:mvc_pattern/mvc_pattern.dart';

import 'package:hand_made/src/controllers/consumer_controller/HomeConsumerDetails1Controller.dart';
import 'package:hand_made/src/widgets/HomeConsumerDetailsCard.dart';



class HomeConsumerDetails1Screen extends StatefulWidget{

createState()=>HomeConsumerDetails1View();
}

class HomeConsumerDetails1View extends StateMVC<HomeConsumerDetails1Screen>{
HomeConsumerDetails1View():super(HomeConsumerDetails1Controller()){
_homeConsumerDetails1Controller = HomeConsumerDetails1Controller.con;
}

HomeConsumerDetails1Controller _homeConsumerDetails1Controller;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      endDrawer: DrawerW().showDrawerUser(context),
       appBar: AppBar(
        centerTitle: true,
        title: Text(
          "مطبخ الاسرة",
          style: TextStyle(color: Colors.grey[800]),
        ),
        backgroundColor: Colors.grey[50],
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back , color: Colors.grey[800],),
          onPressed: ()=>Navigator.pop(context),
        ),
      ),
      body: Column(
        children: <Widget>[

          Expanded(
                  child: GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                    ),
                    physics: AlwaysScrollableScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    itemCount: 6,
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index) {
                      return HomeConsumerDetails1Card();
                    },
                  ),
                )

        ],
      ),
    );
  }


}