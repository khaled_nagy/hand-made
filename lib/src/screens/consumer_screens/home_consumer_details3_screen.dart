import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:hand_made/drawer.dart';

import 'package:mvc_pattern/mvc_pattern.dart';

import 'package:hand_made/src/controllers/consumer_controller/HomeConsumerDetails3Controller.dart';

class HomeConsumerDetails3Screen extends StatefulWidget {
  createState() => HomeConsumerDetails3View();
}

class HomeConsumerDetails3View extends StateMVC<HomeConsumerDetails3Screen> {
  HomeConsumerDetails3View() : super(HomeConsumerDetails3Controller()) {
    _homeConsumerDetails3Controller = HomeConsumerDetails3Controller.con;
  }

  HomeConsumerDetails3Controller _homeConsumerDetails3Controller;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      bottomNavigationBar: 

            Padding(
              padding: const EdgeInsets.only(left: 32 , right: 32 , bottom: 5),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    InkWell(
                      child: new Container(
                        height: 45.0,
                        width: MediaQuery.of(context).size.width / 2.2,
                        child: new Material(
                            color: const Color(0xffA60A53),
                            elevation: 0.0,
                            shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(12.0),
                            ),
                            child: new Center(
                                child: new Padding(
                                    padding: new EdgeInsets.only(
                                        top: 0.0, bottom: 0.0),
                                    child: new Text(
                                      "اطلبة",
                                      style: new TextStyle(
                                          color: Colors.white,
                                          fontSize: 18.0,
                                          fontFamily: 'JF Flat'),
                                    )))),
                      ),
                      onTap: () {
                        Navigator.pop(context);
                      },
                    ),
                   new Text(
                                      "36 ريال",
                                      style: new TextStyle(
                                          color: Colors.grey[800],
                                          fontSize: 18.0,
                                          fontFamily: 'JF Flat'),
                                    )
                  ],
                ),
            ),
            endDrawer: DrawerW().showDrawerUser(context),
      
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "تفاصيل المنتج",
          style: TextStyle(color: Colors.grey[800]),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.grey[800],
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Colors.grey[50],
        elevation: 0,
      ),
      body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Padding(
            padding: const EdgeInsets.only(left: 32, right: 32),
            child: Column(
              children: <Widget>[
                Container(
                  constraints: BoxConstraints.expand(height: 200),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(12)),
                  ),
                  child: new Swiper(
                    itemBuilder: (BuildContext context, int index) {
                      // if(index == this.widget.images.length ){
                      //   return new Image.network(
                      //     UrlHelpers.getImageLink(this.widget.ad.featuredImage),
                      //     fit: BoxFit.cover,
                      //   );
                      // }
                      return new Image.network(
                        "http://via.placeholder.com/350x150",
                        fit: BoxFit.fill,
                      );
                    },
                    autoplay: true,
                    itemCount: 5,
                    pagination: new SwiperPagination(),
                    control:
                        new SwiperControl(color: Colors.blue.withOpacity(0.0)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "كلاسيك هاوس سلايدر",
                    style: TextStyle(color: Colors.grey[800], fontSize: 18),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16),
                  child: Row(
                    children: <Widget>[
                      Text(
                        "برجر هاوس",
                        style: TextStyle(
                            color: const Color(0xffA60A53), fontSize: 18),
                      ),
                    ],
                  ),
                ),
                Text(
                  "حيث ننتقل من تعريفكم بالفن الى ارساله لمنازلكم مباشرة ! هذا المتجر خاص بموقع وقناة الاعمال اليدوية للجميع .",
                  style: TextStyle(color: Colors.grey[500], fontSize: 15),
                ),
                Divider(),
                Padding(
                  padding: const EdgeInsets.only(top: 20, bottom: 20),
                  child: Row(
                    children: <Widget>[
                      Text(
                        "نوع التوصيل",
                        style: TextStyle(color: Colors.grey[800], fontSize: 18),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 32.0, right: 32.0),
                        child: Text(
                          " توصيل بأجر  10 ريال",
                          style:
                              TextStyle(color: Colors.grey[500], fontSize: 15),
                        ),
                      ),
                    ],
                  ),
                ),
                Divider(),
                Padding(
                  padding: const EdgeInsets.only(top: 20, bottom: 20),
                  child: Row(
                    children: <Widget>[
                      Text(
                        "وقت التسليم",
                        style: TextStyle(color: Colors.grey[800], fontSize: 18),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 32.0, right: 32.0),
                        child: Text(
                          " ساعة الى ساعتين ",
                          style:
                              TextStyle(color: Colors.grey[500], fontSize: 15),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
