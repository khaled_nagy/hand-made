import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:hand_made/drawer.dart';
import 'package:hand_made/src/screens/consumer_screens/home_consumer_details1_screen.dart';

import 'package:mvc_pattern/mvc_pattern.dart';

import 'package:hand_made/src/controllers/consumer_controller/HomeConsumerController.dart';
import 'package:hand_made/src/widgets/HomeConsumerCard.dart';

class HomeConsumerScreen extends StatefulWidget {
  createState() => HomeConsumerView();
}

class HomeConsumerView extends StateMVC<HomeConsumerScreen> {
  HomeConsumerView() : super(HomeConsumerController()) {
    _homeConsumerController = HomeConsumerController.con;
  }

  HomeConsumerController _homeConsumerController;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      endDrawer: DrawerW().showDrawerUser(context),
      body: Padding(
        padding: const EdgeInsets.only(left: 32, right: 32, top: 50),
        child: Column(
          children: <Widget>[
            Container(
              constraints: BoxConstraints.expand(height: 200),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(12)),
              ),
              child: Container(
                  height: 200.0,
                  width: 300.0,
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(12)),
                  child: new Carousel(
                    borderRadius: true,
                    moveIndicatorFromBottom: -10,
                    
                    
                    images: [
                      new ExactAssetImage('assets/imgs/ads1.png'),
                      new ExactAssetImage('assets/imgs/ads2.png'),
                      new ExactAssetImage("assets/imgs/ads3.png"),
                    ],
                  )),
            ),
Container(height: 20,),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(children: <Widget>[Text("افضل المتاجر" , style: TextStyle(fontSize: 18 , color: Colors.grey[800]),)]),
            ),

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
height: 100,
width: MediaQuery.of(context).size.width,
child: ListView.builder(
  itemCount: 6,
  shrinkWrap: true,
  physics: AlwaysScrollableScrollPhysics(),
  scrollDirection: Axis.horizontal,
  itemBuilder: (BuildContext context , int index){
    return Container(
      width: 100,
      child: Column(
        children: <Widget>[
          CircleAvatar(
         backgroundColor: Colors.white,
              radius: 30,
              child: Image.asset("assets/imgs/ic_cat_1.png"),

          ),
          Text("اسم المتجر"),

        ],
      ),
    );
  },
),
              ),
            ),
            Expanded(
              child: GridView.count(
                physics: AlwaysScrollableScrollPhysics(),
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                crossAxisCount: 2,
                children: <Widget>[
                  InkWell(
                    child: Card(
                      child: Container(
                        width: MediaQuery.of(context).size.width / 2.2,
                        height: MediaQuery.of(context).size.height / 3,
                        child: Column(
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height / 6,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(12)),
                              child: Image.asset("assets/imgs/ic_cat_1.png"),
                            ),
                            Text(
                              "مطبخ الاسرة",
                              style: TextStyle(
                                  fontSize: 12, color: Colors.grey[800]),
                            ),
                          ],
                        ),
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  HomeConsumerDetails1Screen()));
                    },
                  ),
                  InkWell(
                    child: Card(
                      child: Container(
                        width: MediaQuery.of(context).size.width / 2.2,
                        height: MediaQuery.of(context).size.height / 3,
                        child: Column(
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height / 6,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(12)),
                              child: Image.asset("assets/imgs/ic_cat_2.png"),
                            ),
                            Text(
                              "صالون الاسرة",
                              style: TextStyle(
                                  fontSize: 12, color: Colors.grey[800]),
                            ),
                          ],
                        ),
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  HomeConsumerDetails1Screen()));
                    },
                  ),
                  InkWell(
                    child: Card(
                      child: Container(
                        width: MediaQuery.of(context).size.width / 2.2,
                        height: MediaQuery.of(context).size.height / 3,
                        child: Column(
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height / 6,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(12)),
                              child: Image.asset("assets/imgs/ic_cat_3.png"),
                            ),
                            Text(
                              "مشغولات يدوية",
                              style: TextStyle(
                                  fontSize: 12, color: Colors.grey[800]),
                            ),
                          ],
                        ),
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  HomeConsumerDetails1Screen()));
                    },
                  ),
                  InkWell(
                    child: Card(
                      child: Container(
                        width: MediaQuery.of(context).size.width / 2.2,
                        height: MediaQuery.of(context).size.height / 3,
                        child: Column(
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height / 6,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(12)),
                              child: Image.asset("assets/imgs/ic_cat_4.png"),
                            ),
                            Text(
                              "ضيافة الاسرة",
                              style: TextStyle(
                                  fontSize: 12, color: Colors.grey[800]),
                            ),
                          ],
                        ),
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  HomeConsumerDetails1Screen()));
                    },
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
