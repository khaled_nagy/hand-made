import 'package:mvc_pattern/mvc_pattern.dart';

class SearchConsumerController extends ControllerMVC{


  factory SearchConsumerController() {
    if (_this == null) _this = SearchConsumerController._();
    return _this;
  }
  static SearchConsumerController _this;

  SearchConsumerController._();

  static SearchConsumerController get con => _this;
}