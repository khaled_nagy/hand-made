import 'package:mvc_pattern/mvc_pattern.dart';

class PFR_Controller extends ControllerMVC{


  factory PFR_Controller() {
    if (_this == null) _this = PFR_Controller._();
    return _this;
  }
  static PFR_Controller _this;

  PFR_Controller._();

  static PFR_Controller get con => _this;
}