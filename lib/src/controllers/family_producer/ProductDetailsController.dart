import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class ProductDetailsController extends ControllerMVC {
  factory ProductDetailsController() {
    if (_this == null) _this = ProductDetailsController._();
    return _this;
  }
  static ProductDetailsController _this;

  ProductDetailsController._();

  static ProductDetailsController get con => _this;

  Widget openBottomSheetDelete(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Text(
                "شغل يدوى بالكروشية رائع",
                style: TextStyle(color: Colors.grey[500], fontSize: 15),
              ),
              Text(
                "هل انت متاكد من حذف المنتج ؟",
                style: TextStyle(color: Colors.grey[800], fontSize: 18),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  InkWell(
                    child: new Container(
                      height: 45.0,
                      width: MediaQuery.of(context).size.width / 2.2,
                      child: new Material(
                          color: const Color(0xffA60A53),
                          elevation: 0.0,
                          shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(12.0),
                          ),
                          child: new Center(
                              child: new Padding(
                                  padding: new EdgeInsets.only(
                                      top: 0.0, bottom: 0.0),
                                  child: new Text(
                                    "لا",
                                    style: new TextStyle(
                                        color: Colors.white,
                                        fontSize: 18.0,
                                        fontFamily: 'JF Flat'),
                                  )))),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                  InkWell(
                    child: new Container(
                      decoration: BoxDecoration(
                          border: Border.all(width: 1, color: Colors.grey[500]),
                          borderRadius: BorderRadius.circular(12)),
                      height: 45.0,
                      width: MediaQuery.of(context).size.width / 2.2,
                      child: new Material(
                          color: Colors.grey[50],
                          elevation: 0.0,
                          shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(12.0),
                          ),
                          child: new Center(
                              child: new Padding(
                                  padding: new EdgeInsets.only(
                                      top: 0.0, bottom: 0.0),
                                  child: new Text(
                                    "نعم",
                                    style: new TextStyle(
                                        color: Colors.grey[800],
                                        fontSize: 18.0,
                                        fontFamily: 'JF Flat'),
                                  )))),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              )
            ],
          );
        });
  }
}
