import 'package:mvc_pattern/mvc_pattern.dart';

class Login1Controller extends ControllerMVC{


  factory Login1Controller() {
    if (_this == null) _this = Login1Controller._();
    return _this;
  }
  static Login1Controller _this;

  Login1Controller._();

  static Login1Controller get con => _this;
}